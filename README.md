# common-ci-tasks-images

This project contains container images used by [`common-ci-tasks`](https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks).

## Images Maintained in this Repository

* [`container-diff`](Dockerfile.container-diff): [`registry.gitlab.com/gitlab-com/gl-infra/common-ci-tasks-images/container-diff`](https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks-images/container_registry/5967027)
* [`cyclonedx-syft`](Dockerfile.cyclonedx-syft): [`registry.gitlab.com/gitlab-com/gl-infra/common-ci-tasks-images/cyclonedx-syft`](https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks-images/container_registry/7986897)
* [`docker`](Dockerfile.docker): [`registry.gitlab.com/gitlab-com/gl-infra/common-ci-tasks-images/docker`](https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks-images/container_registry/6695787)
* [`go-jsonnet`](Dockerfile.go-jsonnet): [`registry.gitlab.com/gitlab-com/gl-infra/common-ci-tasks-images/go-jsonnet`](https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks-images/container_registry/6483302)
* [`golang-fips`](Dockerfile.golang-fips): [`registry.gitlab.com/gitlab-com/gl-infra/common-ci-tasks-images/golang-fips`](https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks-images/container_registry/8372397)
* [`golangci-lint`](Dockerfile.golangci-lint): [`registry.gitlab.com/gitlab-com/gl-infra/common-ci-tasks-images/golangci-lint`](https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks-images/container_registry/3917059)
* [`goreleaser`](Dockerfile.goreleaser): [`registry.gitlab.com/gitlab-com/gl-infra/common-ci-tasks-images/goreleaser`](https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks-images/container_registry/6728820)
* [`goreleaser-golang-fips`](Dockerfile.goreleaser-golang-fips): [`registry.gitlab.com/gitlab-com/gl-infra/common-ci-tasks-images/goreleaser-golang-fips`](https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks-images/container_registry/6799192)
* [`hclfmt`](Dockerfile.hclfmt): [`registry.gitlab.com/gitlab-com/gl-infra/common-ci-tasks-images/hclfmt`](https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks-images/container_registry/4014255)
* [`jq`](Dockerfile.jq): [`registry.gitlab.com/gitlab-com/gl-infra/common-ci-tasks-images/jq`](https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks-images/container_registry/4175147)
* [`semantic-release`](Dockerfile.semantic-release): [`registry.gitlab.com/gitlab-com/gl-infra/common-ci-tasks-images/semantic-release`](https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks-images/container_registry/6760188)
* [`shellcheck`](Dockerfile.shellcheck): [`registry.gitlab.com/gitlab-com/gl-infra/common-ci-tasks-images/shellcheck`](https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks-images/container_registry/3346679)
* [`shfmt`](Dockerfile.shfmt): [`registry.gitlab.com/gitlab-com/gl-infra/common-ci-tasks-images/shfmt`](https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks-images/container_registry/3346676)
* [`terraform-module-publish`](Dockerfile.terraform-module-publish): [`registry.gitlab.com/gitlab-com/gl-infra/common-ci-tasks-images/terraform-module-publish`](https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks-images/container_registry/8001700)
* [`tflint`](Dockerfile.tflint): [`registry.gitlab.com/gitlab-com/gl-infra/common-ci-tasks-images/tflint`](https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks-images/container_registry/3346678)
* [`vendir`](Dockerfile.vendir): [`registry.gitlab.com/gitlab-com/gl-infra/common-ci-tasks-images/vendir`](https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks-images/container_registry/6956443)
* [`yamlfmt`](Dockerfile.yamlfmt): [`registry.gitlab.com/gitlab-com/gl-infra/common-ci-tasks-images/yamlfmt`](https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks-images/container_registry/8046005)

## Hacking on common-ci-tasks-images

### Preparing your Environment

1. Follow the developer setup guide at <https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/docs/developer-setup.md>.
1. Clone this project
1. Run `scripts/prepare-dev-env.sh`
