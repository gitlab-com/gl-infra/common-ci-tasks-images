#!/usr/bin/env bash

#######################################
# This script rebuilds the YAML matrices
#######################################

set -euo pipefail
IFS=$'\n\t'

REPO_DIR=$(cd "$(dirname "${BASH_SOURCE[0]}")/.." && pwd)

tmpdir=$(mktemp -d)
trap 'rm -rf "${tmpdir}"' EXIT

main() {
  update_goreleaser_cross_go_versions
}

# This script fetches the 100 most recent "go1.*" tags from the golang-fips repository and
# parses out the version numbers. The 6 newest versions will be used in
# the compile go versions.
# This information is then inserted into the .gitlab-ci.yml.
# Source: https://github.com/golang-fips/go/tags
function update_goreleaser_cross_go_versions() {
  # go-fips versions
  query=$(
    cat <<EOD | jq --slurp --raw-input '{query: .}'
  query {
    repository(owner:"golang-fips", name:"go") {
      refs(refPrefix:"refs/tags/", query: "go1.", first:100, orderBy: { field: TAG_COMMIT_DATE, direction: DESC}) {
        nodes { name }
      }
    }
  }
EOD
  )

  response=$(curl --fail --silent -H "Authorization: bearer $RENOVATE_GITHUB_TOKEN" \
    -H 'Content-Type: application/json' \
    -X POST \
    -d "$query" \
    https://api.github.com/graphql | jq -rc '
    def extract_version(v): v | ltrimstr("go") | sub("-.*"; "");

    [
          .data.repository.refs.nodes[] |
          select(.name | contains("openssl-fips")) |
          { branch: .name, go_version: extract_version(.name) }
    ] |

    reduce .[] as $item ({}; . + { (extract_version($item.branch)): ([.[extract_version($item.branch)],$item.branch]|max) }) |
    [to_entries[] | { go_version: .key, branch: .value }]
    ' | jq -rc '
    def minor(v): v | split(".")[0:2] | join(".");

    # given to semvers, returns the highest
    def max_semver(a;b):
      [a//"",b//""]|sort_by(split(".") | map(tonumber))|last;

    # figure out what the latest patch release for each minor release is, to use for the alias
    (reduce .[] as $item ({}; . + { (minor($item.go_version)): max_semver(.[minor($item.go_version)]; $item.go_version)})) as $latest |

    # tag the image as the MAJOR.MINOR if its the latest patch for the minor version
    [
      .[] | . as $d |
      .extra_args |= (if $latest[minor($d.go_version)] == $d.go_version then "--tag $CI_REGISTRY_IMAGE/${BUILD_NAME}:\(minor($d.go_version))" else "" end)
    ] |

    # SemVer sort cleverness lifted from
    # https://stackoverflow.com/questions/65728085/jq-sort-by-version-as-string
    sort_by(.go_version | split(".") | map(tonumber)) |
    reverse[:6] |
    .[]')

  echo "$response" | jq -r '"          - \(.go_version)"' >"${tmpdir}/goreleaser_cross_go_versions.yml"
  echo "$response" | jq -r '
      ["      - GOLANG_FIPS_BRANCH: \"\(.branch)\"",
        "        GO_VERSION: \"\(.go_version)\""
      ] + (
        # No empty strings in parallel:matrix
        # https://gitlab.com/gitlab-org/gitlab/-/issues/396318
        if .extra_args != "" then ["        DOCKER_EXTRA_ARGS_DESTINATIONS: \"\(.extra_args)\""] else [] end
      ) | join("\n")
    ' >"${tmpdir}/golang_fips_versions.yml"

  replace_marker_section "goreleaser_cross_go_versions" "${tmpdir}/goreleaser_cross_go_versions.yml" "${REPO_DIR}/.gitlab-ci.yml"
  replace_marker_section "golang_fips_versions" "${tmpdir}/golang_fips_versions.yml" "${REPO_DIR}/.gitlab-ci.yml"
}

function replace_marker_section() {
  local section_name=$1
  local snippet_source=${2}
  local file=${3}
  local tmpoutput
  tmpoutput=$(mktemp)

  awk -v snippet_source="${snippet_source}" '
    /\# MARKER:'"$section_name"'/ {
      in_marker = 1;
      saw_marker = 1;
      print;
      while ((getline line < snippet_source) > 0)
        print line
      close(snippet_source)
    }

    /\# END_MARKER:'"$section_name"'/ {
      in_marker = 0;
    }

    // {
      if (in_marker != 1) { print }
    }

    END {
      if (in_marker == 1) {
        print "No close marker" > "/dev/stderr"
        exit 1
      }
      if (saw_marker != 1) {
        print "Open marker was not encountered" > "/dev/stderr"
        exit 1
      }
    }
  ' "${file}" >"${tmpoutput}"

  mv "${tmpoutput}" "${file}"
}

main "$@"
