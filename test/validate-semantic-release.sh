#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

echo "semantic-release --verify-conditions"
semantic-release --verify-conditions
