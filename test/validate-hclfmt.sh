#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

echo "hclfmt version"
hclfmt --version
