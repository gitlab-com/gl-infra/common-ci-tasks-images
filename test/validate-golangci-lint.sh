#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

echo "golangci-lint version"
golangci-lint --version
