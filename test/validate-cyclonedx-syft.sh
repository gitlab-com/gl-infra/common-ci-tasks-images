#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

echo "cosign version"
cosign version

echo "cyclonedx --version"
cyclonedx --version

echo "syft --version"
syft --version

echo "jq version"
jq --version

echo "yq version"
yq --version
