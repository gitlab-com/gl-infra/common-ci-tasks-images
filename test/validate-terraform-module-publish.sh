#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

echo "tar --version"
tar --version
