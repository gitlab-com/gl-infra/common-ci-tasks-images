#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

set -x

jsonnet --version
jsonnetfmt --version
jsonnet-deps --version
jsonnet-lint --version
