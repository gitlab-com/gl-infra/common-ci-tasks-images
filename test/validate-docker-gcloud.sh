#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

echo "docker version"
docker --version

echo "cosign version"
cosign version

echo "yq version"
yq --version

echo "gcloud version"
gcloud --version
