#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

echo "shellcheck version"
shellcheck --version
