#!/usr/bin/env bash

set -euo pipefail
set -x

yamlfmt -version
git --version
