#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

echo "goreleaser --version"
goreleaser --version

echo "cosign version"
cosign version

echo "gcc --version"
gcc --version

echo "syft --version"
syft --version

echo "go version"
go version

[[ -e /usr/local/bin/go ]] || {
  echo "/usr/local/bin/go does not exist"
  exit 1
}

echo "docker version"
docker version
