#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

echo "goreleaser --version"
goreleaser --version

echo "cosign version"
cosign version

echo "gcc --version"
gcc --version

echo "syft --version"
syft --version

echo "docker version"
docker version
