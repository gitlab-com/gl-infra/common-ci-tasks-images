#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

echo "jq version"
jq --version

echo "numfmt validation"
numfmt --to=iec-i --suffix=B --format="%9.2f" 1000000

echo "docker version"
docker --version
