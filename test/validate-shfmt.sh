#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

echo "shfmt version"
shfmt --version
