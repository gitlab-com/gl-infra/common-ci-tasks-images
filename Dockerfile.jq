FROM alpine:3.21.3

ARG GL_BUILD_JQ_VERSION

RUN apk add --no-cache grep bash pcre

RUN wget https://github.com/stedolan/jq/releases/download/jq-${GL_BUILD_JQ_VERSION}/jq-linux64 -O /usr/bin/jq && \
  chmod 755 /usr/bin/jq

CMD [ "/bin/bash" ]
